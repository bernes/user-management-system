# Project Name

## Features

- Establishment of database schema and predefined data.
- CRUD functionality for each entity (client, user, city).
- Creation of templates for data visualization and registration.
- Implementation of pagination to handle large volumes of data.
- Addition of a filter to search clients by city.
- Ability to export client data to Excel format.
- Implementation of user sessions and login functionality.
- Generation of a JWT-authenticated REST API to access the data.

## Description

This project involves the development of a client management application based on cities. The application utilizes a database with three tables: `client`, `users`, and `cities`. The following are the main features of the application:

1. **Establishment of database schema and predefined data**: Defines the structure of the database and provides a predefined set of initial data.

2. **CRUD functionality for each entity**: Enables creating, reading, updating, and deleting records for the `client`, `user`, and `city` entities.

3. **Creation of templates for data visualization and registration**: Implements templates that allow intuitive and efficient display and registration of information about clients, users, and cities.

4. **Implementation of pagination**: Includes a pagination mechanism to handle large volumes of data, facilitating navigation and optimizing application performance.

5. **Filter clients by city**: Adds the ability to filter and display only clients associated with a specific city, enhancing the user experience when working with specific data.

6. **Export client data to Excel format**: Provides a function to export client data to an Excel file, facilitating analysis and further use.

7. **User sessions and login functionality**: Implements a user session system that requires authentication to access application functionalities, ensuring security and access control.

8. **Generation of a JWT-authenticated REST API**: Creates a REST API that allows interaction with the application data, using JWT authentication to ensure the security of requests and protect data integrity.

## Requirements

- Python 3.9
- Check requirements.txt file
- Database: sqlite

## Installation

1. Clone this repository.

2. Build the image

```bash
    docker build -t serempre .
```

3. Run the image
```
    docker run -d --name serempre -p 80:80 serempre
```

## Usage

1. Access the application through http://127.0.0.1/docs.

2. There is the swagger for review the CRUD endpoints and REST API

3. Use the http://127.0.0.1/user/login endpoint to login and get acces token

4. Token needed to access to http://127.0.0.1/user/info for update or get an user

5. Access the template http://127.0.0.1/cities/crud to use the city CRUD template
