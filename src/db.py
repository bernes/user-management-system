import random

from fastapi import HTTPException
from sqlmodel import create_engine, select, Session, SQLModel

from src.models import (cities, clients, users)  # noqa F401
from src.settings import URI


engine = create_engine(URI, echo=True)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def save_to_db(session: Session, model: SQLModel):
    session.add(model)
    session.commit()
    session.refresh(model)
    return model


def get_all_from_db(session: Session, model: SQLModel, offset: int, limit: int):
    return session.exec(select(model).offset(offset).limit(limit)).all()


def get_from_db(session: Session, entity: SQLModel, entity_id: int) -> SQLModel:
    entity_db = session.get(entity, entity_id)
    if not entity_db:
        raise HTTPException(status_code=404,
                            detail=f"{entity.__name__} not found")
    return entity_db


def generate_data():
    try:
        with Session(engine).no_autoflush as session:
            for i in range(1, 11):
                city = cities.City(name=f"City {i}")
                save_to_db(session, city)
                user = users.User(name=f"User {i}",
                                  password=f"pass{i}",
                                  email=f"user{i}@app.com")
                save_to_db(session, user)
            for j in range(1, 100):
                client = clients.Client(name=f"Client {j}",
                                        city_id=random.randint(1, 10),
                                        user_id=random.randint(1, 10))
                save_to_db(session, client)
    except Exception:
        return
