from fastapi import APIRouter, Depends, Query, status
from fastapi.responses import FileResponse, StreamingResponse
from fastapi.security import OAuth2PasswordRequestForm
import pandas as pd
from sqlmodel import Session

from src.dependencies import db, token
from src.db import get_from_db, get_all_from_db, save_to_db
from src.models import users
from src.security import Security, Token
from src.schemas.clients import Client
from src.schemas.users import User, UserCreate, UserUpdate
from src.services.users import get_clients_by_user, get_clients_by_user_city
from src.utils import update_entity
from src.validations.users import email_is_unique, verify_authenticate_user


router = APIRouter(prefix="/user", tags=["user"])


@router.post("/", response_model=User)
def create_user(user: UserCreate, session: Session = Depends(db.get_session)):
    email_is_unique(session, user.email)
    hashed_password = Security.get_password_hash(user.password)
    user.password = hashed_password
    db_user = users.User.from_orm(user)
    return save_to_db(session, db_user)


@router.get("/", response_model=list[User])
def get_users(session: Session = Depends(db.get_session),
              offset: int = 0,
              limit: int = Query(default=5, lte=100)):
    return get_all_from_db(session, users.User, offset, limit)


@router.get("/info", response_model=User)
def get_user(session: Session = Depends(db.get_session),
             user_logged: users.User = Depends(token.validate_token)):
    return user_logged


@router.put("/info", response_model=User)
def update_user(user: UserUpdate,
                session: Session = Depends(db.get_session),
                user_logged: users.User = Depends(token.validate_token)):
    if user.email:
        email_is_unique(session, user.email)
    user_updated = update_entity(user, user_logged)
    return save_to_db(session, user_updated)


@router.delete("/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(user_id: int, session: Session = Depends(db.get_session)):
    user_db = get_from_db(session, users.User, user_id)
    session.delete(user_db)
    session.commit()
    return


@router.post("/login")
def login(form_data: OAuth2PasswordRequestForm = Depends(),
          session: Session = Depends(db.get_session)):
    email = form_data.username
    password = form_data.password
    user = verify_authenticate_user(session, email, password)
    data = {"user_id": user.id}
    access_token = Token.create_access_token(data)
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/info/{user_id}/clients/", response_model=list[Client])
def get_clients_from_city(user_id: str,
                          city: str = Query(default=None),
                          session: Session = Depends(db.get_session)):
    if city:
        return get_clients_by_user_city(user_id, city, session)
    return get_clients_by_user(user_id, session)


def streamfile():
    with open("exported_excel.xlsx", mode="rb") as excel_file:
        yield from excel_file


@router.post("/info/{user_id}/clients/", response_class=FileResponse)
def get_clients_from_city_to_excel(user_id: str,
                                   city: str = Query(default=None),
                                   session: Session = Depends(db.get_session)):
    clients = get_clients_by_user_city(user_id, city, session) if city else get_clients_by_user(user_id, session)  # noqa E501
    clients_dict = [client.__dict__ for client in clients]
    pd.DataFrame(clients_dict).to_excel("exported_excel.xlsx",
                                        columns=list(Client.__fields__.keys()),
                                        index=False)
    headers = {'Content-Disposition': 'attachment; filename="clients.xlsx"'}
    return StreamingResponse(streamfile(), headers=headers)
