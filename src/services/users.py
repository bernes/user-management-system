from sqlmodel import select, Session

from src.db import get_from_db
from src.models import clients, users


def get_clients_by_user(user_id: int, session: Session):
    user_db = get_from_db(session, users.User, user_id)
    return list(user_db.clients)


def get_clients_by_user_city(user_id: int, city: int, session: Session):
    statement = select(clients.Client).where(
        clients.Client.user_id == user_id).where(
        clients.Client.city_id == city)
    clients_filter = session.exec(statement)
    return list(clients_filter)
