from fastapi import HTTPException, status
from sqlmodel import select, Session

from src.models.users import User
from src.security import Security


def email_is_unique(session: Session, email: str):
    if session.exec(select(User).where(User.email == email)).first():
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            "Email already exist")


def verify_authenticate_user(session: Session, email: str, password: str):
    user = session.exec(select(User).where(User.email == email)).first()
    if not user:
        raise HTTPException(status_code=400,
                            detail="Incorrect username or password")
    if not Security.verify_password(password, user.password):
        raise HTTPException(status_code=400,
                            detail="Incorrect username or password")
    return user
