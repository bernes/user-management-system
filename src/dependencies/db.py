from sqlmodel import Session

from src.db import engine


def get_session():
    with Session(engine).no_autoflush as session:
        yield session
