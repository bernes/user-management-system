from typing import Optional

from sqlmodel import Field, SQLModel


class City(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(max_length=150, unique=True)
    population: int = Field()
