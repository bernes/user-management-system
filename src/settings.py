from fastapi.templating import Jinja2Templates


# Load the database credentials
sqlite_file_name = "database.db"
URI = f"sqlite:///{sqlite_file_name}"


# Token
SECRET_KEY = "ashdfkasjdkfjgiireq"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 60.0


# Templates
templates = Jinja2Templates(directory="templates")
