from typing import Annotated

from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, Form, Request, status
from fastapi.responses import HTMLResponse, RedirectResponse

from src.dependencies import db
from src.db import get_from_db, get_all_from_db, save_to_db
from src.models import cities
from src.schemas.cities import City, CityCreate, CityUpdate
from src.settings import templates
from src.utils import update_entity
from src.validations.cities import cityname_is_unique


router = APIRouter(prefix="/cities", tags=["cities"])


@router.get("/crud", response_class=HTMLResponse)
def get_cities_crud(request: Request,
                    session: Session = Depends(db.get_session),
                    offset: int = 0,
                    limit: int = Query(default=5, lte=100)):
    cities_ = get_all_from_db(session, cities.City, offset, limit)
    return templates.TemplateResponse("cityindex.html",
                                      {"request": request, "cities_": cities_})


@router.post("/crud", response_class=HTMLResponse)
def create_city_crud(name: Annotated[str, Form()],
                     population: Annotated[str, Form()],
                     session: Session = Depends(db.get_session)):
    cityname_is_unique(session, name)
    db_city = cities.City(name=name, population=population)
    save_to_db(session, db_city)
    return RedirectResponse(url="/cities/crud", status_code=status.HTTP_303_SEE_OTHER)


@router.get("/crud/add")
def addnew(request: Request):
    return templates.TemplateResponse("cityadd.html", {"request": request})


@router.post("/crud/update/{city_id}")
def update_city_crud(city_id: int,
                     name: Annotated[str, Form()],
                     session: Session = Depends(db.get_session)):
    city_db = get_from_db(session, cities.City, city_id)
    cityname_is_unique(session, name)
    city_updated = update_entity(CityUpdate(name=name), city_db)
    save_to_db(session, city_updated)
    return RedirectResponse(url="/cities/crud", status_code=status.HTTP_303_SEE_OTHER)


@router.get("/crud/edit/{city_id}")
def edit_city_crud(request: Request, city_id: int, session: Session = Depends(db.get_session)):
    city_db = get_from_db(session, cities.City, city_id)
    return templates.TemplateResponse("cityupdate.html", {"request": request, "city": city_db})


@router.get("/crud/delete/{city_id}")
def delete(city_id: int, session: Session = Depends(db.get_session)):
    city_db = get_from_db(session, cities.City, city_id)
    session.delete(city_db)
    session.commit()
    return RedirectResponse(url="/cities/crud", status_code=status.HTTP_303_SEE_OTHER)


@router.post("/", response_model=City)
def create_city(city: CityCreate, session: Session = Depends(db.get_session)):
    cityname_is_unique(session, city.name)
    db_city = cities.City.from_orm(city)
    return save_to_db(session, db_city)


@router.get("/", response_model=list[City])
def get_cities(session: Session = Depends(db.get_session),
               offset: int = 0,
               limit: int = Query(default=5, lte=100)):
    return get_all_from_db(session, cities.City, offset, limit)


@router.get("/{city_id}", response_model=City)
def get_city(city_id: int, session: Session = Depends(db.get_session)):
    return get_from_db(session, cities.City, city_id)


@router.put("/{city_id}", response_model=City)
def update_city(city_id: int,
                city: CityUpdate,
                session: Session = Depends(db.get_session)):
    if city.name:
        cityname_is_unique(session, city.name)
    city_db = get_from_db(session, cities.City, city_id)
    city_updated = update_entity(city, city_db)
    return save_to_db(session, city_updated)


@router.delete("/{city_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_city(city_id: int, session: Session = Depends(db.get_session)):
    city_db = get_from_db(session, cities.City, city_id)
    session.delete(city_db)
    session.commit()
    return status.HTTP_204_NO_CONTENT
