from typing import Optional

from pydantic import BaseModel


class CityBase(BaseModel):
    name: str
    population: int


class CityCreate(CityBase):
    pass


class City(CityBase):
    id: int

    class Config:
        orm_mode = True


class CityUpdate(BaseModel):
    name: Optional[str]
    population: Optional[int]
