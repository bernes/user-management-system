from typing import Optional, TYPE_CHECKING

from sqlmodel import Field, Relationship, SQLModel

if TYPE_CHECKING:
    from src.models.clients import Client


class User(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(max_length=150)
    password: str = Field(max_length=150)
    email: str = Field(max_length=150, unique=True)
    photo: Optional[str] = Field(default=None, max_length=300)
    clients: list["Client"] = Relationship()
