from fastapi import FastAPI

from src.db import create_db_and_tables, generate_data
from src.routers import cities, clients, users


app = FastAPI()

app.include_router(cities.router)
app.include_router(clients.router)
app.include_router(users.router)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()
    generate_data()
