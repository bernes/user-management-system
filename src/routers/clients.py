from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, status

from src.dependencies import db, token
from src.db import get_from_db, get_all_from_db, save_to_db
from src.models import cities, clients, users
from src.schemas.clients import Client, ClientCreate, ClientUpdate
from src.utils import update_entity


router = APIRouter(prefix="/clients", tags=["clients"])


@router.post("/", response_model=Client)
def create_client(client: ClientCreate,
                  session: Session = Depends(db.get_session),
                  user: users.User = Depends(token.validate_token)):
    city = get_from_db(session, cities.City, client.city_id)
    db_client = clients.Client.from_orm(client, {"user_id": user.id})
    db_client.user = user
    db_client.city = city
    return save_to_db(session, db_client)


@router.get("/", response_model=list[Client])
def get_clients(session: Session = Depends(db.get_session),
                offset: int = 0,
                limit: int = Query(default=5, lte=100)):
    return get_all_from_db(session, clients.Client, offset, limit)


@router.get("/{client_id}", response_model=Client)
def get_client(client_id: int, session: Session = Depends(db.get_session)):
    return get_from_db(session, clients.Client, client_id)


@router.put("/{client_id}", response_model=Client)
def update_client(client_id: int,
                  client: ClientUpdate,
                  session: Session = Depends(db.get_session)):
    client_db = get_from_db(session, clients.Client, client_id)
    if client.city_id:
        client_db.city = get_from_db(session, cities.City, client.city_id)
    client_updated = update_entity(client, client_db)
    return save_to_db(session, client_updated)


@router.delete("/{client_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_client(client_id: int, session: Session = Depends(db.get_session)):
    client_db = get_from_db(session, clients.Client, client_id)
    session.delete(client_db)
    session.commit()
    return
