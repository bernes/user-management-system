from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError
from jose.exceptions import ExpiredSignatureError
from sqlmodel import Session

from src.dependencies.db import get_session
from src.models.users import User
from src.security import Token


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="user/login")


def validate_token(session: Session = Depends(get_session),
                   token: str = Depends(oauth2_scheme)):
    try:
        payload = Token.decode_token(token)
    except JWTError as error:
        if error == ExpiredSignatureError:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Token has expired")
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Could not validate credentials"
            )
    user_id = payload.get("user_id")
    user = session.get(User, user_id)
    if not user:
        raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Could not validate credentials"
            )
    return user
